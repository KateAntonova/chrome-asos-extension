'use strict';
console.log('script started');

var tokenUpdateTime = new Date();

var HOST = 'https://www.asos.com';
var API_PATH = 'api/commerce/bag/v3/bags';
var bagId = '';
var authorization = '';
var client_id = 'D91F2DAA-898C-4E10-9102-D6C974AFBD59';
var itemId = '';
var variantId = '';
var allItems = [];
var INTERVAL_TIME = 1000 * 10; //10sec

function setBagId() {

    let localData = localStorage.getItem('Asos.Commerce.Bag.Sdk');
    let localDataToObj = JSON.parse(localData);
    let localDataObjProp = Object.values(localDataToObj);

    bagId = localDataObjProp[0].data.bagId;
    console.log('bagId = ', bagId);
}

// function setAuthToken() {
//     let localData = localStorage.getItem('Asos.TokenManager.token');
//     let localDataToObj = JSON.parse(localData);
//     let localDataObjProp = Object.values(localDataToObj);
//     console.log('localDataObjProp: ', localDataObjProp)
//     authorization = 'bearer ' + localDataObjProp[2];
//     // console.log('authorization = ', authorization);
// }

function refreshToken() {

    let localData = localStorage.getItem('Asos.TokenManager.token');
    let localDataToObj = JSON.parse(localData);
    let localDataObjProp = Object.values(localDataToObj);
    let Id_Token = localDataObjProp[1];

    var xhr = new XMLHttpRequest();
    xhr.open("POST", `
    https://my.asos.com/identity/oauth/v2/token/exchange?lang=ru-RU&store=RU&country=RU&keyStoreDataversion=5e950e2a-9
    `, false);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    let data = `Id_Token=${Id_Token}&client_id=${client_id}&grant_type=urn:ietf:params:oauth:grant-type:token-exchange&assertion_format=JWT&scope=sensitive`;
    xhr.send(data);

    if (xhr.status === 200 || xhr.status === 201) {
        authorization = 'bearer ' + JSON.parse(xhr.responseText).access_token
        console.log('access_token = ', JSON.parse(xhr.responseText))
    }
}

function createReq(verb, url) {
    let xhr = new XMLHttpRequest();
    xhr.open(verb, url, false);
    xhr.setRequestHeader('Authorization', authorization);
    xhr.setRequestHeader('Content-Type', 'application/json');
    return xhr;
}

function timeStatistics() {
    let fromLastTokenUpdate = (new Date() - tokenUpdateTime);
    tokenUpdateTime = new Date();
    return Math.round(fromLastTokenUpdate / 1000 / 60);
}

function getBag(successFunc) {

    let getReq = createReq(
        'GET',
        `${HOST}/${API_PATH}/${bagId}?expand=summary,total,get`
    );
    getReq.send();

    if (getReq.status !== 200 && getReq.status !== 201) {
        console.log('Auth err, restart. After the last token update passed', timeStatistics(), 'sec');
        authorization = '';
        setBagId();
        refreshToken();

    } else {
        allItems = JSON.parse(getReq.responseText).bag.items;
        console.log('all bag items = ', allItems);
        successFunc()
    }
}

function putToBag() {

    let putReq = createReq(
        'POST',
        `${HOST}/${API_PATH}/${bagId}/product?expand=summary,total&lang=ru-RU`
    );
    putReq.send(JSON.stringify({variantId: variantId}));

    if (putReq.status !== 200 && putReq.status !== 201) {
        console.log('Auth err, restart. After the last token update passed', timeStatistics(), 'sec');
        authorization = '';
        setBagId();
        refreshToken();

    } else {
        console.log('put to Bag item with ID = ', variantId)
    }
}

function removeFromBag(successFunc) {

    let removeReq = createReq(
        'DELETE',
        `${HOST}/${API_PATH}/${bagId}/product/${itemId}?expand=summary,total,getdeliveryoptions,spendLevelDiscount,address,expiredItems&lang=ru-RU`
    );
    removeReq.send();

    if (removeReq.status !== 200 && removeReq.status !== 201) {
        console.log('Auth err, restart. After the last token update passed', timeStatistics(), 'sec');
        authorization = '';
        setBagId();
        refreshToken();

    } else {
        successFunc();
    }
}

function delAndPut() {
    getBag(() => {
        allItems.forEach(item => {
            itemId = item.id;
            variantId = item.variantId;
            removeFromBag(putToBag);
        })
    });
}

refreshToken();
setBagId();
setInterval(delAndPut, INTERVAL_TIME);



